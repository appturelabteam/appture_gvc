<?php

namespace Appture\GVC;

class Controller {
    static private $router;
    static private $controller_list;
    static private $path = "controllers";
    protected $route_name;
    
    function __construct($route_name) {
        $this->route_name = $route_name;
    }
    
    static function setRouter($router) {
        Controller::$router = $router;
    }
    
    static function getControllers() {
        return Controller::$controller_list;
    }

    /*
     * this function is called by the router to run the controller
     * 
     * the ultimate return should be get_defined_vars(); 
     * 
     * remember to add this return to an override or called function so you can access these variables from the rendered view
     */
    
    function run($params) {
        $methods = get_class_methods(get_class($this));
        
        if( !isset($params[1]) || (strpos($params[1],"getRouteName")===false && 
                strpos($params[1],"setRouteName")===false && 
                strpos($params[1],"includeAll")===false &&
                strpos($params[1],"_")!==0) ) { // dont allow pre-underscores in method names to be called
            //print "Looking for member func...";
            if(array_search($params[1], $methods)!==false) { // if a safe member function is found call it
                //print " found one.";
                return call_user_func(array($this, $params[1]), $params);
            } else if(array_search("view", $methods)!==false) { // fall back to the view member function if it exists
                //print " not found, call view func.";
                return call_user_func(array($this, "view"), $params);
            }
            //print " found none.";
        }
        
        return false;
    }
    
    function getRouteName() {
        return $this->route_name;
    }
    
    function setRouteName($route_name) {
        $this->route_name=$route_name;
    }
    
    static function setPath($path) {
        Controller::$path = $path;
    }
    
    
    static function includeAll() {
        //print print_r(Controller::$controller_list,true)."<br>";
        
        // rearrange by weight
        $weightedControllers = array();
        foreach(Controller::$controller_list as $class=>$controller) {
            if(!isset($weightedControllers[$controller["weight"]])) {
                $weightedControllers[$controller["weight"]] = array();
            }
            $weightedControllers[$controller["weight"]][$class] = $controller;
        }
        
        ksort($weightedControllers);
        
        foreach($weightedControllers as $weight=>$weightGroup) {
            //ksort($weightGroup);
            //print "Group: $weight<br>";
            foreach($weightGroup as $class=>$controller) {
                
                include(Controller::$path."/".$class.".php");
                $controllerInstance = new $class($controller["route"]);
                
                $newController = array(array());
                
                // only run install or update functions when DS is available
                if(class_exists("DS")) {
                    $methods = get_class_methods($class);
                    //print "Version".$class::$version."<br>";
                    
                    // run installer if foreced or if not installed yet
                    if($controller["version"] === -1) {
                        if(array_search("install", $methods) !== false) {
                            //call_user_func(array($class, "install"));
                            try {
                                $controllerInstance->install();
                            } catch (Exception $e) {
                                error_log($e->getMessage().PHP_EOL.$e->getTraceAsString());
                                break 2;
                            }
                        }
                    }
                }
                
                //$controller["lastAccess"] = date(DATE_ISO8601);
                if(!isset($controller["id"])) {

                    $controller["version"] = 0;
                    $controller["run_post"] = 1;
                    $newController = DS::insert("controllers", $controller);
                    
                }
                
                $controller["instance"] = $controllerInstance;
                
                Controller::$controller_list[$class] = array_merge($controller, $newController[0]);
            }
        }
        
        // only run install or update functions when DS is available
        if(class_exists("DS")) {
            
            // initiate postInstall
            foreach($weightedControllers as $weight=>$weightGroup) {
                //ksort($weightGroup);
                //print "Group: $weight<br>";
                foreach($weightGroup as $class=>$controller) {
                    
                    $controller = Controller::$controller_list[$class];
                    
                    if(isset($controller["instance"])) {
                        
                        $methods = get_class_methods($class);
                        //print "Version".$class::$version."<br>";
                        
                        // get the latest value of run_post
                        $runPost = DS::select("controllers", "WHERE id = ?i", $controller["id"]);

                        if($runPost[0]["run_post"] == "1") {
                        
                            if(array_search("postInstall", $methods) !== false) {
                                //call_user_func(array($class, "install"));
                                $controller["instance"]->postInstall();
                            }
                            
                            // set update_in_progress in db so we won't run update while already busy
                            DS::update("controllers", array("run_post" => 0), "WHERE id = ?i", $controller["id"]);
                        }

                    }
                }
            }
            
            // initiate updates
            foreach($weightedControllers as $weight=>$weightGroup) {
                //ksort($weightGroup);
                //print "Group: $weight<br>";
                foreach($weightGroup as $class=>$controller) {

                    $controller = Controller::$controller_list[$class];

                    if(isset($controller["instance"])) {

                        $methods = get_class_methods($class);
                        //print "Version".$class::$version."<br>";

                        // run update if version has updates
                        if($class::$version !== 0 && $controller["version"] < $class::$version) {

                            if(array_search("update", $methods) !== false) {

                                // get the latest value of update_in_progress
                                $updating = DS::select("controllers", "WHERE id = ?i", $controller["id"]);

                                if($updating[0]["update_in_progress"] == "0") {

                                    // set update_in_progress in db so we won't run update while already busy
                                    DS::update("controllers", array("update_in_progress" => 1), "WHERE id = ?i", $controller["id"]);

                                    // run the update
                                    $newVersion = Controller::$controller_list[$class]["instance"]->update($controller["version"]);

                                    // has the update failed?
                                    if($newVersion !== false) {
                                        $controller["version"] = $newVersion;
                                        if(isset($controller["id"])) {
                                            DS::update("controllers", array("version" => $newVersion, "update_in_progress" => 0), "WHERE id = ?i", $controller["id"]);
                                        }
                                    } else {
                                        DS::update("controllers", array("update_in_progress" => 0), "WHERE id = ?i", $controller["id"]);
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    static function addRoute($route, $class, $weight) {
        $controller = array(
            "controller_class" => $class,
            "version" => -1,
            "weight" => $weight,
            "route" => $route
        );
        
        if(class_exists("DS")) {
            if(count($controllers = DS::select("controllers","WHERE controller_class = ?s", $class))) {
                $controller = $controllers[0];
            }
        }
        
        Controller::$controller_list[$class] = $controller;
        Controller::$router->set($route,$class);
    }
    
    static function includeSingle($controllerClass) {
        Controller::loadData($controllerClass);
        //print print_r(Controller::$controller_list,true)."<br>";
        
        include(Controller::$path."/".$controllerClass.".php");

        // only run install or update functions when DS is available
        if(class_exists("DS")) {
            $methods = get_class_methods($controllerClass);
            //print "Version".$controllerClass::$version."<br>";
            // run installer if foreced or if not installed yet
            if(Controller::$controller_list[$controllerClass]["version"] === -1) {
                if(array_search("install", $methods) !== false && class_exists("DS")) {
                    call_user_func(array($controllerClass, "install"));
                    Controller::$controller_list[$controllerClass]["version"] = 0;
                }
            }
            
            if(!isset(Controller::$controller_list[$controllerClass]["id"])) {
                if(class_exists("DS")) {
                    $newController = DS::insert("controllers", Controller::$controller_list[$controllerClass]);
                    Controller::$controller_list[$controllerClass] = $newController[0];
                }
            }

            // run update if version has updates
            if($controllerClass::$version !== 0 && Controller::$controller_list[$controllerClass]["version"] < $controllerClass::$version) {
                
                if(array_search("update", $methods) !== false) {
                    
                    // get the latest value
                    $updating = DS::select("controllers", "WHERE id = ?i", Controller::$controller_list[$controllerClass]["id"]);
                    
                    if($updating[0]["update_in_progress"] == "0") {

                        // set update_in_progress in db so we won't run update while already busy
                        DS::update("controllers", array("update_in_progress" => 1), "WHERE id = ?i", Controller::$controller_list[$controllerClass]["id"]);

                        // run the update
                        $newVersion = call_user_func(array($controllerClass, "update"),Controller::$controller_list[$controllerClass]["version"]);

                        // has the update failed?
                        if($newVersion !== false) {
                            Controller::$controller_list[$controllerClass]["version"] = $newVersion;
                            if(isset(Controller::$controller_list[$controllerClass]["id"])) {
                                DS::update("controllers", Controller::$controller_list[$controllerClass], "WHERE id = ?i", Controller::$controller_list[$controllerClass]["id"]);
                            }
                        } else {
                            DS::update("controllers", array("update_in_progress" => 0), "WHERE id = ?i", Controller::$controller_list[$controllerClass]["id"]);
                        }
                        
                    }
                    
                }
            }
        }
    }
    
    static function loadAllData($readFolder = false) {
        $found_controllers = array();
        
        if(class_exists("DS")) {
            $controllers = DS::select("controllers","ORDER BY weight");
            Controller::$controller_list = array();
            foreach($controllers as $controller) {
                if(file_exists(Controller::$path."/".$controller["controller_class"].".php")) {
                    $found_controllers[] = $controller["controller_class"];
                    Controller::$controller_list[$controller["controller_class"]] = $controller;
                } else {
                    //$controller["missing"] = 1;
                    //DS::update("controllers", $controller, "WHERE id = ?i", $controller["id"]);
                }
            }
        }
        
        if($readFolder) {
            if($handle = opendir(Controller::$path)) {
                while (false !== ($entry = readdir($handle))) {
                    if(strpos($entry, ".php") === strlen($entry)-4) {
                        $class = str_replace(".php", "", $entry);
                        if(stripos($entry,".php")!==false) {
                            Controller::$controller_list[$class] = array(
                                "controller_class" => $class,
                                "version" => $class::$version,
                                "weight" => $class::$weight
                            );
                            if(array_search($class, $found_controllers) === false) {
                                Controller::$controller_list[$class]["version"] = -1; // this is first run
                            }
                        }
                    }
                }
                closedir($handle);
            }
        }
    }
    
    static function loadData($controllerClass) {
        if(file_exists($controllerClass.".php") && Controller::$controller_list[$class] === null) {
            Controller::$controller_list[$controllerClass] = array(
                "controller_class" => $controllerClass,
                "version" => -1, // this is first run
                "weight" => $controllerClass::$weight
            );
        }
    }
    
    static function _install() {
        $tables = DS::list_tables();
        
        // check if the users table exists, if not create it.
        if(array_search('controllers',$tables)===false) {
            // generate the create table query
            $query = "CREATE TABLE controllers (
                        id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT, 
                        route VARCHAR(128) NOT NULL,
                        controller_class VARCHAR(128) NOT NULL,
                        version INTEGER UNSIGNED NOT NULL,
                        weight INTEGER NOT NULL,
                        update_date DATETIME,
                        create_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
                        PRIMARY KEY (id)
                    ) ENGINE = InnoDB;";

            if(DS::query($query)) {
                //message_add("The Users table has been created.");
            }
        }
        
        // run an upgrade
        $tableInfo = DS::table_info("controllers");
        if(!isset($tableInfo["route"])) {
            DS::query("ALTER TABLE controllers ADD route VARCHAR(128) NOT NULL AFTER id;");
        }
        if(!isset($tableInfo["update_date"])) {
            DS::query("ALTER TABLE controllers CHANGE updateDate update_date DATETIME;");
        }
        if(!isset($tableInfo["create_date"])) {
            DS::query("ALTER TABLE controllers CHANGE createDate create_date DATETIME;");
        }
        if(!isset($tableInfo["update_in_progress"])) {
            DS::query("ALTER TABLE controllers ADD update_in_progress TINYINT(1) DEFAULT 0 AFTER version;");
        }
        if(!isset($tableInfo["run_post"])) {
            DS::query("ALTER TABLE controllers ADD run_post TINYINT(1) DEFAULT 0 AFTER version;");
        }
        
        DS::clear_table_info("controllers");
    }
    
    function _createJsonResponseBody () {
        return array(
            "message" => "Some parameters are missing.",
            "success" => false,
            "access" => true,
            "versions" => array(
                "api_version" => API_VERSION,
                "version_web" => VERSION_WEB,
                "version_android" => VERSION_ANDROID,
                "version_ios" => VERSION_IOS
            )
        );
    }
    
    function _generateOptions ($genericName, $tableFields, $replaceOptions) {
        return array_replace_recursive(array(
            $genericName => array(
                "GET" => array(
                    "title" => Template::toTitleCase($genericName)." List",
                    "description" => "List resources.",
                    "request" => array(
                        "parameters" => array(
                            "limit" => array("Field" => "limit", "Type" => "int", "Null" => "yes", "Key" => "", "Default" => 25, "Extra" => "", "Description" => "Limit the number of items returned (default: 25)."),
                            "page" => array("Field" => "page", "Type" => "int", "Null" => "yes", "Key" => "", "Default" => 0, "Extra" => "", "Description" => "Page through the returned items (default: 0)."),
                            "filter[]" => array("Field" => "filter[]", "Type" => "varchar", "Null" => "yes", "Key" => "", "Default" => null, "Extra" => "", "Description" => "Filter the returned items ({field_name:value} or {value} for searching in all fields).")
                        ),
                        "body" => array()
                    ),
                    "response" => array(
                        "list" => array($tableFields),
                        "limit" => null,
                        "page" => null,
                        "count" => null,
                        "total_filtered" => null,
                        "total" => null
                    )
                ),
                "POST" => array(
                    "title" => "Create ".Template::toTitleCase($genericName),
                    "description" => "Create a resource.",
                    "request" => array(
                        "parameters" => null,
                        "body" => $tableFields,
                    ),
                    "response" => $tableFields
                ),
            ),
            $genericName."/{id}" => array(
                "GET" => array(
                    "title" => "Get ".Template::toTitleCase($genericName),
                    "description" => "Get a resource.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array()
                    ),
                    "response" => $tableFields,
                ),
                "PUT" => array(
                    "title" => "Update ".Template::toTitleCase($genericName),
                    "description" => "Update a resource. Allows sparse payloads.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => $tableFields
                    ),
                    "response" => $tableFields,
                ),
                "DELETE" => array(
                    "title" => "Delete ".Template::toTitleCase($genericName),
                    "description" => "Permanently remove a resource from the database.",
                    "request" => array(
                        "parameters" => array(
                            "id" => array("Field" => "id", "Type" => "int", "Null" => "no", "Key" => "", "Default" => "", "Extra" => "", "Description" => "Resource identifier"),
                        ),
                        "body" => array()
                    ),
                    "response" => array()
                )
            )
        ),$replaceOptions);
    }
    
    function _getViewer($request) {
        
        //$request = new \Zend\Diactoros\ServerRequest();
        $scopes = $request->getAttribute("oauth_scopes");
        $user_id = $request->getAttribute("oauth_user_id");
        $client_id = $request->getAttribute("oauth_client_id");
        $view_as = $request->getHeader("ViewAs");
        
        /*if(is_array($view_as) && is_array($scopes) && count(array_intersect(["webmaster","administrator"],$scopes)) > 0 && count($view_as) && intval($view_as[0])) {
            $user_id = $view_as[0];
            
            //DS::select("users","WHERE id = ?i",$view_as);
        }*/
        
        // if we are only authenticated as a confidential client, set the clients owner_id as user_id
        if($user_id == 0 && $client_id != "") {
            if(count($client = DS::select(ClientController::$tableName, "WHERE is_active = 1 AND is_confidential = 1 AND identifier = ?s", $client_id)) > 0) {
                $user_id = $client[0]["owner_id"];
        }
        }
        
        return array($scopes,$user_id,$client_id);
    }
    
    /**
     * 
     * @param integer $currentVersion
     * @return integer Must return new version number.
     */
    function update($currentVersion) {
        return $currentVersion;
    }
}