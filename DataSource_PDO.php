<?php

namespace Appture\GVC;

class DS {
    
    // date format
    static public $DATE_DB = "Y-m-d H:i:s";
    
    static private $connections = array();
    static private $default_database = "";
    static private $dao_default_pageLimit = 40;
    static private $page_limit = 0;
    static private $total_rows = null;
    static private $total_filtered_rows = null;
    static private $filter_table = "";
    static private $query_gen = true;
    
    static private $table_info_cache = array();
    static private $table_primary_key_cache = array();
    
    static function connect($host,$user,$pass,$database,$type="MYSQL") {
        // TODO: If you are connecting via TCP/IP rather than a UNIX socket remember to add the port number as a parameter.
        
        $dbh = null;
        try {
            switch ($type) {
                case "MSSQL":
                    //$dbh = new PDO("odbc:Driver={SQL Server};Server={$host};Database={$database};Uid={$user};Pwd={$pass}",$user, $pass);            
                    //$dbh = new PDO("mssql:host={$host};dbname={$database}", $user, $pass);
                    $dbh = new PDO("sqlsrv:Server={$host};Database={$database}", $user, $pass);
                    break;
                case "MYSQL":
                default:
                    $type = "MYSQL";
                    $dbh = new PDO("mysql:host={$host};dbname={$database};charset=utf8", $user, $pass);
            }
        } catch (Exception $e) {
            echo 'Connection failed: ' . $e->getMessage().PHP_EOL;
            return false;
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage().PHP_EOL;
            return false;
        }
        
        DS::$connections[$database]["database"] = $database;
        DS::$connections[$database]["connection"] = $dbh;
        DS::$connections[$database]["type"] = $type;
        DS::$connections[$database]["server"] = $host;
        DS::$table_info_cache[$database] = array();
        DS::$table_primary_key_cache[$database] = array();
        
        if(DS::$default_database=="") {
            DS::$default_database = $database;
        }
    }
    
    static function getCurrentDatabase() {
        return DS::$default_database;
    }

    static function selectDatabase($database) {
        DS::$default_database = $database;
    }
    
    static function get($database="") {
        if($database=="") {
            $database = DS::$default_database;
        }
        return DS::$connections[$database]["connection"];
    }
    
    static function getInfo($database="") {
        if($database=="") {
            $database = DS::$default_database;
        }
        return array(DS::$connections[$database]["database"],DS::$connections[$database]["server"]);
    }
    
    static function close() {
        foreach(DS::$connections as $val) {
            //$val["connection"]->close();
        }
    }
    
    static function setPageLimit($limit) {
        DS::$page_limit = $limit;
    }
    
    static function setFilterTable($table) {
        DS::$filter_table = $table;
    }
    
    static function getTotalRows() {
        return DS::$total_rows;
    }
    
    static function beginTransaction() {
        $connection = DS::get();
        $connection->beginTransaction();
    }
    
    static function commit() {
        $connection = DS::get();
        $connection->commit();
    }
    
    static function rollBack() {
        $connection = DS::get();
        $connection->rollBack();
    }
    
    static function gen_query($query) {
        $arguments = func_get_args();
        
        $queryArgs = array();
        $offset = 0;
        $num = 1;
        $at = stripos($query, "?", $offset);
        $str = substr($query,$at,2);
        //print $query." - ";
        //print $pos = ($at != -1 ? ($str == "?s" ? $at : ($str == "?i" ? $at : ($str == "?f" ? $at : ($str == "?d" ? $at : -1)))) : -1);
        //die();
        while($pos = stripos($query, "?", $offset)) {
            $type = substr($query, $pos+1, 1);
            //print "found at:".$pos."=|".$type."|";
            if($type!="s" && $type!="i" && $type!="f" && $type!="d") {$offset=$pos+1;continue;}
            
            if(!isset($arguments[$num])) {
                die("Error in datasource->query: Incorrect number of arguments, Key({$num}) does not exist.".PHP_EOL." -> query: {$query}".PHP_EOL." -> arguments: ".print_r($arguments,true));
            } else {
                if(is_array($arguments[$num])) {
                    $arrArgs = $arguments[$num];
                    unset($arguments[$num]);
                    $arguments = array_merge($arguments,$arrArgs);
                }
            }
            
            // ?s = strings
            if($type=="s") {
                if(is_object($arguments[$num]))
                    throw new Exception ("Error in query: {$query}".PHP_EOL."Value for key {$num} is an object.");
                $queryArgs[] = DS::escape($arguments[$num]);//$connection->quote($arguments[$num]);// dif between mysql and mssql?
            }
            
            // ?i = integers
            if($type=="i") {
                $queryArgs[] = $arguments[$num] === false ? 0 : intval($arguments[$num]);
            }
            
            // ?f = floats
            if($type=="f") {
                $queryArgs[] = floatval($arguments[$num]);
            }
            
            // ?d = doubles
            if($type=="d") {
                $queryArgs[] = doubleval($arguments[$num]);
            }
            
            $query = substr_replace($query, "#-".($num-1), $pos, 2);
            
            $offset = $pos+1;
            $num++;
            
            $at = stripos($query, "?", $offset);
            $str = substr($query,$at,2);
        }
        
        for($x=0;$x<count($queryArgs);$x++) {
            $query = preg_replace("/#-$x/", $queryArgs[$x], $query, 1);
        }
        
        return $query.";";
    }
    
    /*
     * query($query,$paramaters...)
     * 
     * Secure database query...
     * 
     * Always uses the default database, which can be set via the selectDatabase function.
     */
    static function query($query) {
        $arguments = func_get_args();
        
        $connection = DS::get();
        
        $queryArgs = array();
        $offset = 0;
        $num = 1;
        $space = null;
        $at = stripos($query, "?", $offset);
        $str = substr($query,$at,2);
        //print $query." - ";
        //print $pos = ($at != -1 ? ($str == "?s" ? $at : ($str == "?i" ? $at : ($str == "?f" ? $at : ($str == "?d" ? $at : -1)))) : -1);
        //die();
        while($pos = stripos($query, "?", $offset)) {
            $type = substr($query, $pos+1, 1);
			$space = strlen($query) > $pos+2 ? substr($query, $pos+2, 1) : " ";
            //print $query." found at:".$pos;
			//var_dump($space);

			if($type != "s" && $type != "i" && $type != "f" && $type != "d") {$offset=$pos+1;continue;}
			if($space !== " " && $space !== PHP_EOL && $space !== "\n" && $space !== "\r" && $space !== ")" && $space !== ",")  {$offset=$pos+1;continue;}
            
            if(!isset($arguments[$num])) {
                $type = "null";
                //throw new Exception("Error in datasource->query: Incorrect number of arguments, Key({$num}) does not exist.".PHP_EOL." -> query: {$query}".PHP_EOL." -> arguments: ".print_r($arguments,true));
            } else {
                if(is_array($arguments[$num])) {
                    $arrArgs = $arguments[$num];
                    unset($arguments[$num]);
                    $arguments = array_merge($arguments,$arrArgs);
                }
            }
            
            // ?s = strings
            if($type=="s") {
                if(is_object($arguments[$num]))
                    throw new Exception ("Error in query: {$query}".PHP_EOL."Value for key {$num} is an object.");
                $queryArgs[] = DS::escape($arguments[$num]);//$connection->quote($arguments[$num]);// dif between mysql and mssql?
            }
            
            // ?i = integers
            if($type=="i") {
                $queryArgs[] = $arguments[$num] === false ? 0 : intval($arguments[$num]);
            }
            
            // ?f = floats
            if($type=="f") {
                $queryArgs[] = floatval($arguments[$num]);
            }
            
            // ?d = doubles
            if($type=="d") {
                $queryArgs[] = doubleval($arguments[$num]);
            }
            
            if($type=="null") {
                $queryArgs[] = 'NULL';//$connection->quote($arguments[$num]);// dif between mysql and mssql?
            }
            
            $query = substr_replace($query, "#-".($num-1), $pos, 2);
            
            $offset = $pos+1;
            $num++;
            
            $at = stripos($query, "?", $offset);
            $str = substr($query,$at,2);
        }
        
        for($x=0;$x<count($queryArgs);$x++) {
            $query = preg_replace("/#-$x/", $queryArgs[$x], $query, 1);
        }
        
        /*
        $stmt = DS::get()->query($query);
        if ( !$stmt ) {
            exit( 'Query error: ' . print_r(DS::get()->errorInfo(), true));
        }
        //$stmt->nextRowset();
        do {
            print print_r( $stmt->fetchAll(PDO::FETCH_ASSOC),true)."<br>";
        } while ($stmt->nextRowset());*/
        /*if(stripos($query,"FROM menus WHERE url =")!==false) {
            //Message::addSession($query);
            print $query."<br>";
            //return array("0"=>array("Verified"=>1));
            die("TESTING... YOU SHOULD NOT BE SEING THIS. OOPS!");
        }*/
        
        try {
            //$stmt = new PDOStatement();
            $stmt = $connection->prepare($query);
            if($stmt->execute()) {
                $result = array();
                do {
                    //print "res: ".print_r($stmt->fetchAll(PDO::FETCH_ASSOC),true).PHP_EOL;
                    if(count($fetch = $stmt->fetchAll(PDO::FETCH_ASSOC))) {
                        $result = $fetch;
                    }
                } while ($stmt->nextRowset());
                //print (microtime()-$time_start)."<br>";
                return $result;
            } else {
                throw new Exception("Error in query (last |$space|): {$query}".PHP_EOL." -> ".print_r($stmt->errorInfo(),true));
            }
            return null;
            
        } catch (Exception $e) {
            throw $e;//new Exception($e->getMessage(), $e->getCode());
        }
    }
    
    /**
     * 
     * @param type $fields
     * @param type $filterValue
     * @param type $logicalOperator
     * @param type $fieldPrefix
     * @return type
     */
    static function gen_filters($fields,$filterValue,$logicalOperator = "OR",$fieldPrefix = "") {
        $filterString = "(1=1)";
        
        $filters = array_filter($filterValue);
        
        /*if(count($filters) === 0 && stripos($filterValue,"+") !== false) {
            // multiple filters
            $filters = explode("+",$filterValue);
        }*/
        
        if(count($filters) > 0) {
            
            $filterString = "";
            foreach($filters as $filterGroup) {
                //$filterSubParts = explode(" ",$filterGroup);
                $filter = $filterGroup;
                
                //foreach($filterSubParts as $filter) {
                    $filterPart = "";
                    
                    if(stripos($filter,"LIKE")!==false) { // LIKE
                        // filter on a specific column
                        $columnFilter = explode("LIKE",$filter);
                        $filterVal = DS::escape("%{$columnFilter[1]}%");
                        if(array_search($columnFilter[0],$fields) !== false)
                            $filterPart = ($filterPart !== "" ? " $logicalOperator " : ""). "{$fieldPrefix}{$columnFilter[0]} LIKE $filterVal";

                    } else if(stripos($filter,">")!==false) { // greater
                        // filter on a specific column
                        $columnFilter = explode(">",$filter);
                        $filterVal = DS::escape("{$columnFilter[1]}");
                        if(array_search($columnFilter[0],$fields) !== false)
                            $filterPart = ($filterPart !== "" ? " $logicalOperator " : ""). "{$fieldPrefix}{$columnFilter[0]} > $filterVal";

                    } else if(stripos($filter,">=")!==false) { // greater or equal
                        // filter on a specific column
                        $columnFilter = explode(">=",$filter);
                        $filterVal = DS::escape("{$columnFilter[1]}");
                        if(array_search($columnFilter[0],$fields) !== false)
                            $filterPart = ($filterPart !== "" ? " $logicalOperator " : ""). "{$fieldPrefix}{$columnFilter[0]} >= $filterVal";

                    } else if(stripos($filter,"<")!==false) { // less
                        // filter on a specific column
                        $columnFilter = explode("<",$filter);
                        $filterVal = DS::escape("{$columnFilter[1]}");
                        if(array_search($columnFilter[0],$fields) !== false)
                            $filterPart = ($filterPart !== "" ? " $logicalOperator " : ""). "{$fieldPrefix}{$columnFilter[0]} < $filterVal";

                    } else if(stripos($filter,"<=")!==false) { // less or equal
                        // filter on a specific column
                        $columnFilter = explode("<=",$filter);
                        $filterVal = DS::escape("{$columnFilter[1]}");
                        if(array_search($columnFilter[0],$fields) !== false)
                            $filterPart = ($filterPart !== "" ? " $logicalOperator " : ""). "{$fieldPrefix}{$columnFilter[0]} <= $filterVal";

                    } else if(stripos($filter,"<>")!==false) { // not equal
                        // filter on a specific column
                        $columnFilter = explode("<>",$filter);
                        $filterVal = DS::escape("{$columnFilter[1]}");
                        if(array_search($columnFilter[0],$fields) !== false)
                            $filterPart = ($filterPart !== "" ? " $logicalOperator " : ""). "{$fieldPrefix}{$columnFilter[0]} <> $filterVal";

                    } else if(stripos($filter,"!=")!==false) { // not equal
                        // filter on a specific column
                        $columnFilter = explode("!=",$filter);
                        $filterVal = DS::escape("{$columnFilter[1]}");
                        if(array_search($columnFilter[0],$fields) !== false)
                            $filterPart = ($filterPart !== "" ? " $logicalOperator " : ""). "{$fieldPrefix}{$columnFilter[0]} != $filterVal";

                    } else if(stripos($filter,"=")!==false) { // exact match
                        // filter on a specific column
                        $columnFilter = explode("=",$filter);
                        $filterVal = DS::escape("{$columnFilter[1]}");
                        if(array_search($columnFilter[0],$fields) !== false)
                            $filterPart = ($filterPart !== "" ? " $logicalOperator " : ""). "{$fieldPrefix}{$columnFilter[0]} = $filterVal";

                    } else {
                        // filter on all columns
                        $filterVal = DS::escape("%$filter%");
                        foreach($fields as $field) {
                            $filterPart.= ($filterPart !== "" ? " $logicalOperator " : ""). "{$fieldPrefix}{$field} LIKE $filterVal";
                        }

                    }
                    
                    if($filterPart) {
                        $filterString.= ($filterString !== "" ? " $logicalOperator " : ""). "($filterPart)";
                    }
                //}
            }
            
            if($filterString) {
                $filterString = "($filterString)";
            } else {
                $filterString = "(1=1)";
            }
        }
        
        return $filterString;
    }
    
    static function list_tables() {
        $query = "SHOW TABLES";
        $tables = array();
        $result = DS::query($query);
        if($result) {
            foreach($result as $row) {
                $tables[] = $row["Tables_in_".DS::$default_database];
            }
        }
        return $tables;
    }
    
    static function clear_table_info($table) {
        if(isset(DS::$table_info_cache[DS::$default_database][$table])) {
            unset(DS::$table_info_cache[DS::$default_database][$table]);
        }
    }
    
    static function table_info($table) {
        // Field      | Type     | Null | Key | Default | Extra
        if(!isset(DS::$table_info_cache[DS::$default_database][$table])) {
            if(DS::$connections[DS::$default_database]["type"]==="MSSQL") {
                $query = "SELECT 
                            o.name as 'Table',
                            c.name as 'Field',
                            t.Name as 'Type',
                            c.max_length as 'Max Length',
                            c.precision ,
                            c.scale ,
                            c.is_nullable as 'Null',
                            ISNULL(i.is_primary_key, 0) as 'Key',
                            c.is_identity as 'IsIdentity'
                        FROM    
                            sys.objects o
                        INNER JOIN
                            sys.columns c ON o.object_id = c.object_id
                        INNER JOIN 
                            sys.types t ON c.system_type_id = t.system_type_id
                        LEFT OUTER JOIN 
                            sys.index_columns ic ON ic.object_id = c.object_id AND ic.column_id = c.column_id
                        LEFT OUTER JOIN 
                            sys.indexes i ON ic.object_id = i.object_id AND ic.index_id = i.index_id
                        WHERE
                            c.object_id = OBJECT_ID('$table')";
                $result = DS::query($query);
                if($result) {
                    $fields = array();
                    //print_r($result);
                    foreach($result as $row) {
                        $row["Extra"] = ($row["IsIdentity"] == 1 && $row["Key"] == 1 ? "auto_increment" : ""); // fake the MYSQL auto_increment value
                        $row["Default"] = "";
                        $fields[$row['Field']]=$row;
                        if($row["Extra"] == "auto_increment") {
                            DS::$table_primary_key_cache[DS::$default_database][$table] = $row["Field"];
                        }
                    }
                    DS::$table_info_cache[DS::$default_database][$table] = $fields;
                    return $fields;
                }
            } else if(DS::$connections[DS::$default_database]["type"]==="MYSQL") {
                if(!isset(DS::$table_info_cache[DS::$default_database][$table])) {
                    $query = "SHOW COLUMNS FROM ".DS::$default_database.".{$table}";
                    $result = DS::query($query);
                    if($result) {
                        $fields = array();
                        foreach($result as $row) {
                            $fields[$row['Field']]=$row;
                            if($row["Extra"] == "auto_increment") {
                                DS::$table_primary_key_cache[DS::$default_database][$table] = $row["Field"];
                            }
                        }
                        DS::$table_info_cache[DS::$default_database][$table] = $fields;
                        return $fields;
                    }
                }
            }
        } else {
            return DS::$table_info_cache[DS::$default_database][$table];
        }
        return null;
    }
    
    /*
     * $extras = extra query string items ie WHERE, ORDER BY and LIMIT
     * Returns an array even if single result; on no result returns null.
     */
    static function select($table,$extras="") {
        //$limit = "";
        
        /*$hasWhere = (stripos($extras,"WHERE") !== false);
        if($hasWhere) { // remove the where clause, we add to allow the filters
            $extras = str_ireplace($extras, "WHERE", "");
        }
        
        if(DS::$page_limit !== 0) {
            // automatically page the loaded items
            $page=0;
            if(isset($_GET['page'])) {
                $page=$_GET['page'];
            }
            
            $limit = "LIMIT ".($page*(DS::$page_limit==-1 ? DS::$dao_default_pageLimit : DS::$page_limit)).",".(DS::$page_limit==-1 ? DS::$dao_default_pageLimit : DS::$page_limit);
        }
        
        $filterString = "";
        if(DS::$filter_table === $table) {
            $filter = filter_input(INPUT_GET, "filter");
            if($filter) {
                $fields = DS::table_info($table);
                $filter = DS::escape("%$filter%");
                foreach(array_keys($fields) as $field) {
                    $filterString.= ($filterString !== "" ? " OR " : ""). "$field LIKE $filter";
                }
                $filterString ="($filterString)".($hasWhere ? " AND " : "");

                DS::setFilterTable("");
            }
        }*/
        
        $args = func_get_args();
        array_splice($args, 0, 2); // remove the first 2 items from the array
        /*
        // when using limit, calc total rows
        DS::$total_rows = null;
        if($limit !== "") {
            if(count($total_rows = DS::query("SELECT count(*) as total FROM {$table} ".($hasWhere ? "WHERE" : ""). " {$extras}", $args))) {
                DS::$total_rows = intval($total_rows[0]["total"]);
            }
        }*/
        
        return DS::query("SELECT * FROM {$table} {$extras}", ...$args);
    }

    /*
     * helper function for paged lists to be able to know how many results their are
     */
    static function count($table,$extras="") {
        $query = "SELECT COUNT(*) FROM {$table} {$extras}";
        $result = DS::query($query);
        if($result) {
            $row = $result[0];
            return $row['COUNT(*)'];
        }
        return null;
    }
    
    static function gen_insert($table,$fieldData) {
        $query = "INSERT INTO {$table} ";

        $fields = DS::table_info($table);
        
        // pop unnecessary fields and check some values
        foreach($fieldData as $key=>$value) {
            if(stripos($fields[$key]['Extra'],'auto_increment') !== false) {
                unset($fieldData[$key]);
            }
            if(stripos($fields[$key]['Type'],'int')!==false && ($value==="" || ((is_array($value) && !count($value)) || (!is_array($value) && $value=="" )))) {
                if(stripos($fields[$key]['Null'],'no') !== false) {
                    $fieldData[$key] = 0;
                } else {
                    $fieldData[$key] = null;
                }
            }
            if(stripos($fields[$key]['Null'],'no') === false && (stripos($fields[$key]['Type'],'timestamp')!==false || stripos($fields[$key]['Type'],'datetime')!==false) && $value==="") {
                $fieldData[$key] = null;
            }
            if(stripos($fields[$key]['Type'],'tinyint') === 0 || stripos($fields[$key]['Type'],'bool') === 0) {
                $fieldData[$key] = $fieldData[$key] ? 1 : 0;
            }
        }
        
        $fieldsString = "";
        foreach($fieldData as $key=>$value) {
            if($fieldsString!="") {
                $fieldsString.= ", ";
            }
            
            $fieldsString.= "{$key}";
        }
        $query.= "($fieldsString)";
        
        $valuesString = "";
        foreach($fieldData as $key=>$value) {
            if($valuesString!="") {
                $valuesString.= ", ";
            }
            
            // some debugging
            if(is_array($value)) {
                $value = implode(",",$value);
            }
            
            if(is_object($value))
                throw new Exception ("Error in query: {$query}".PHP_EOL."{$key} is an object.");
            $valuesString.= ($value === null ? 'null' : DS::escape($value));
            
        }
        $query.= " VALUES($valuesString)";
        
        $args = func_get_args();
        array_splice($args, 0, 2); // remove the first 2 items from the array
        
        return DS::gen_query($query,$args);
    }
    
    static function gen_insert_multiple($table,$groupedFieldData) {
        $query = "INSERT INTO {$table} ";

        $fields = DS::table_info($table);
        
        $groupedValuesString = array();
        foreach($groupedFieldData as $x=>$fieldData) {
            // pop unnecessary fields and check some values
            foreach($fieldData as $key=>$value) {
                if(stripos($fields[$key]['Extra'],'auto_increment') !== false) {
                    unset($fieldData[$key]);
                }
                if(stripos($fields[$key]['Type'],'int')!==false && ($value==="" || ((is_array($value) && !count($value)) || (!is_array($value) && $value=="" )))) {
                    if(stripos($fields[$key]['Null'],'no') !== false) {
                        $fieldData[$key] = 0;
                    } else {
                        $fieldData[$key] = null;
                    }
                }
                if(stripos($fields[$key]['Null'],'no') === false && (stripos($fields[$key]['Type'],'timestamp')!==false || stripos($fields[$key]['Type'],'datetime')!==false) && $value==="") {
                    $fieldData[$key] = null;
                }
                if(stripos($fields[$key]['Type'],'tinyint') === 0 || stripos($fields[$key]['Type'],'bool') === 0) {
                    $fieldData[$key] = $fieldData[$key] ? 1 : 0;
                }
            }
            
            if($x === 0) {
                $fieldsString = "";
                foreach($fieldData as $key=>$value) {
                    if($fieldsString!="") {
                        $fieldsString.= ", ";
                    }

                    $fieldsString.= "{$key}";
                }
                $query.= "($fieldsString)";
            }
            
            $valuesString = "";
            foreach($fieldData as $key=>$value) {
                if($valuesString!="") {
                    $valuesString.= ", ";
                }

                // some debugging
                if(is_array($value)) {
                    $value = implode(",",$value);
                }

                if(is_object($value))
                    throw new Exception ("Error in query: {$query}".PHP_EOL."{$key} is an object.");
                $valuesString.= ($value === null ? 'null' : DS::escape($value));

            }
            $groupedValuesStrings[] = $valuesString;
            
        }
        
        $query.= " VALUES(".implode("), (",$groupedValuesStrings).")";
        
        $args = func_get_args();
        array_splice($args, 0, 2); // remove the first 2 items from the array
        
        return DS::gen_query($query,$args);
    }

    static function insert($table,$fieldData) {
        $query = "INSERT INTO {$table} ";

        $fields = DS::table_info($table);
        
        // pop unnecessary fields and check some values
        foreach($fieldData as $key=>$value) {
            if(stripos($fields[$key]['Extra'],'auto_increment') !== false) {
                unset($fieldData[$key]);
            }
            if(stripos($fields[$key]['Type'],'int')!==false && ($value==="" || ((is_array($value) && !count($value)) || (!is_array($value) && $value=="" )))) {
                if(stripos($fields[$key]['Null'],'no') !== false) {
                    $fieldData[$key] = 0;
                } else {
                    $fieldData[$key] = null;
                }
            }
            if(stripos($fields[$key]['Null'],'no') === false && (stripos($fields[$key]['Type'],'timestamp')!==false || stripos($fields[$key]['Type'],'datetime')!==false) && $value==="") {
                $fieldData[$key] = null;
            }
            if(stripos($fields[$key]['Type'],'tinyint') === 0 || stripos($fields[$key]['Type'],'bool') === 0) {
                $fieldData[$key] = $fieldData[$key] ? 1 : 0;
            }
        }
        
        $fieldsString = "";
        foreach($fieldData as $key=>$value) {
            if($fieldsString!="") {
                $fieldsString.= ", ";
            }
            
            $fieldsString.= "{$key}";
        }
        $query.= "($fieldsString)";
        
        $valuesString = "";
        foreach($fieldData as $key=>$value) {
            if($valuesString!="") {
                $valuesString.= ", ";
            }
            
            // some debugging
            if(is_array($value)) {
                $value = implode(",",$value);
            }
            
            if(is_object($value))
                throw new Exception ("Error in query: {$query}".PHP_EOL."{$key} is an object.");
            $valuesString.= ($value === null ? 'null' : DS::escape($value));
        }
        $query.= " VALUES($valuesString)";
        
        $args = func_get_args();
        array_splice($args, 0, 2); // remove the first 2 items from the array
        
        $result = DS::query($query,$args);
        if($result!==null) {
            if(isset(DS::$table_primary_key_cache[DS::$default_database][$table])) {
                if(DS::$connections[DS::$default_database]["type"]==="MSSQL") {
                    if($result = DS::query("SELECT * FROM {$table} WHERE ".DS::$table_primary_key_cache[DS::$default_database][$table]."=IDENT_CURRENT('{$table}')")) {
                        return $result;
                    }
                } else if(DS::$connections[DS::$default_database]["type"]==="MYSQL") {
                    if($result = DS::query("SELECT * FROM {$table} WHERE ".DS::$table_primary_key_cache[DS::$default_database][$table]."=LAST_INSERT_ID()")) {
                        return $result;
                    }
                }
            }
            return true;
        }
        return null;
    }
    
    static function gen_update($table,$fieldData,$extras) {
        $query = "UPDATE {$table} SET ";
        
        $fields = DS::table_info($table);
        
        // if an updateDate column exists, insert the current date and time
        if(isset($fields["update_date"])) {
            $fieldData["update_date"] = date(DATE_DB);
        }
        
        // pop unnecessary fields and check some values
        foreach($fieldData as $key=>$value) {
            if(stripos($fields[$key]['Extra'],'auto_increment') !== false) {
                unset($fieldData[$key]);
            }
            
            if(stripos($fields[$key]['Type'],'int')===0 && ($value==="" || ((is_array($value) && !count($value)) || (!is_array($value) && $value=="" )))) {
                if(stripos($fields[$key]['Null'],'no') !== false) {
                    $fieldData[$key] = 0;
                } else {
                    $fieldData[$key] = null;
                }
            }
            
            if(stripos($fields[$key]['Null'],'no') === false && (stripos($fields[$key]['Type'],'timestamp')!==false || stripos($fields[$key]['Type'],'datetime')!==false) && $value==="") {
                $fieldData[$key] = null;
            }
            
            if(stripos($fields[$key]['Type'],'tinyint') === 0 || stripos($fields[$key]['Type'],'bool') === 0) {
                $fieldData[$key] = $fieldData[$key] ? 1 : 0;
            }
        }
        
        if(!count($fieldData)) {
            // no fields to update?
            return true;
        }
        
        $fieldsSets = "";
        foreach($fieldData as $key=>$value) {
            if($fieldsSets != "") {
                $fieldsSets.= ", ";
            }
            
            
            if(is_object($value))
                throw new Exception ("Error in query: {$query}".PHP_EOL."{$key} is an object.");
            $fieldsSets.= "{$key}=".(is_array($value) ? DS::escape(implode(",",$value)) : ($value === null ? 'null' : DS::escape($value)))."";
            
        }
        
        $query.= $fieldsSets." {$extras}";
        
        //if(isset($fieldData['roles'])) {
        //die("q:{$query}"."</br>");
        
        $args = func_get_args();
        array_splice($args, 0, 3); // remove the first 3 items from the array
        
        return DS::gen_query($query,$args);
    }

    static function update($table,$fieldData,$extras) {
        $query = "UPDATE {$table} SET ";
        
        $fields = DS::table_info($table);
        
        // if an updateDate column exists, insert the current date and time
        if(isset($fields["update_date"])) {
            $fieldData["update_date"] = date(DATE_DB);
        }
        
        // pop unnecessary fields and check some values
        foreach($fieldData as $key=>$value) {
            if(stripos($fields[$key]['Extra'],'auto_increment') !== false) {
                unset($fieldData[$key]);
            }
            
            if(stripos($fields[$key]['Type'],'int')===0 && ($value==="" || ((is_array($value) && !count($value)) || (!is_array($value) && $value==="" )))) {
                if(stripos($fields[$key]['Null'],'no') !== false) {
                    $fieldData[$key] = 0;
                } else {
                    $fieldData[$key] = null;
                }
            }
            if(stripos($fields[$key]['Null'],'no') === false && (stripos($fields[$key]['Type'],'timestamp')!==false || stripos($fields[$key]['Type'],'datetime')!==false) && $value==="") {
                $fieldData[$key] = null;
            }
            if(stripos($fields[$key]['Type'],'tinyint') === 0 || stripos($fields[$key]['Type'],'bool') === 0) {
                $fieldData[$key] = $fieldData[$key] ? 1 : 0;
            }
        }
        
        if(!count($fieldData)) {
            // no fields to update?
            return true;
        }
        
        $fieldsSets = "";
        foreach($fieldData as $key=>$value) {
            if($fieldsSets != "") {
                $fieldsSets.= ", ";
            }
            
            if(is_object($value))
                throw new Exception ("Error in query: {$query}".PHP_EOL."{$key} is an object.");
            $fieldsSets.= "{$key}=".(is_array($value) ? DS::escape(implode(",",$value)) : ($value === null ? 'null' : DS::escape($value)))."";
        }
        
        $query.= $fieldsSets." {$extras}";
        
        //if(isset($fieldData['roles'])) {
        //die("q:{$query}"."</br>");
        
        $args = func_get_args();
        array_splice($args, 0, 3); // remove the first 3 items from the array
        
        $result = DS::query($query,$args);
        
        return $result;
    }

    static function delete($table,$extras) {
        $query = "DELETE FROM {$table} ". $extras;
        
        $args = func_get_args();
        array_splice($args, 0, 2); // remove the first 2 items from the array
        
        return DS::query($query,$args);
    }
    
    static function escape($value) {
        $result = DS::get()->quote($value);
        // poor-mans workaround for the fact that not all drivers implement quote()
        if (empty($result))
        {
            $result = "'".str_replace("'", "''", $value)."'";
        }
        return $result;
    }
}