<?php

namespace Appture\GVC;

class Mailer {
    static public $lastError = null;
    
    static function send($to, $cc, $bcc, $from, $subject, $message, $replyto = null, $files = array()) {
        // use swift for dev
        /*if(API_MODE === API_MODE_DEVELOPMENT) {
            $to = "";
            $cc = "";
            $bcc = "";
            return Mailer::swiftSend($to, $cc, $bcc, $from, $subject, $message, $replyto, $files);
        }*/
        
        //if(API_MODE === API_MODE_DEVELOPMENT) {
            return Mailer::swiftSend($to,$cc,$bcc,$from,$subject,$message, $replyto, $files);
        //}
        
        /*$xheaders = "";
        $xheaders .= "From: <$from>\n";
        $xheaders .= "X-Sender: <$from>\n";
        $xheaders .= "X-Mailer: PHP\n"; // mailer
        $xheaders .= "X-Priority: 3\n"; //1 Urgent Message, 3 Normal
        $xheaders .= "Content-Type:text/html; charset=\"iso-8859-1\"\n";
        $xheaders .= "Bcc:{$bcc}\n";
        $xheaders .= "Cc:{$cc}\n";
        return mail($to, $subject, $message, $xheaders);*/
    }
    
    static function swiftSend($to,$cc,$bcc,$from,$subject,$message, $replyto = null, $files = array()) {
        
        // Create the Transport
        // SmtpTransport is usefull for dev setups
        //$transport = new Swift_SmtpTransport('smtp.host', 465, 'ssl');
        //$transport->setUsername('username');
        //$transport->setPassword('password');
        
        /*
        You could alternatively use a different transport such as Sendmail:
        */
        // Sendmail
        $transport = new Swift_SendmailTransport('/usr/sbin/sendmail -bs');

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);
        
        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
        $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));
        
        // SPLIT THE INPUT
        $from = is_array($from) ? $from : array_filter(explode(",",$from));
        $to = is_array($to) ? $to : array_filter(explode(",",$to));
        $cc = is_array($cc) ? $cc : array_filter(explode(",",$cc));
        $bcc = is_array($bcc) ? $bcc : array_filter(explode(",",$bcc));
        
        // Create a message
        $swiftMessage = new Swift_Message($subject);
        $swiftMessage->setFrom($from);
        $swiftMessage->setTo($to);
        
        if($replyto) {
            $swiftMessage->addReplyTo($replyto);
        }
        
        if(count($cc)) {
            $swiftMessage->setCc($cc);
        }
        
        if(count($bcc)) {
            $swiftMessage->setBcc($bcc);
        }
        
        // embed inline images
        $doc = new DOMDocument();
        $doc->loadHTML($message);
        
        $imgs = $doc->getElementsByTagName("img");
        foreach($imgs as $img){
            //print $img->getAttribute('src')." - ".realpath($img->getAttribute('src'))."-<br>";
            if(file_exists(realpath($img->getAttribute('src')))) {
                $image_data = Mailer::dataUri(realpath($img->getAttribute('src')));
                $image = new Swift_Image($image_data["data"], $img->getAttribute('src'), $image_data["mime"]);
                $cid = $swiftMessage->embed($image);
                $img->setAttribute( 'src' , $cid );
            }
        }
        
        //print $doc->saveHTML();
        //die();
        
        $swiftMessage->setBody($doc->saveHTML(),'text/html');
        
        $files = array_filter($files);
        foreach($files as $file) {
            $swiftMessage->attach(Swift_Attachment::fromPath($file));
        }
        
        $result = false;
        
        try {

            $result = $mailer->send($swiftMessage);
            
        } catch(Exception $exception) {
            
        }
        
        Mailer::$lastError = $logger->dump();
        
        return $result;
    }
    
    static function dataUri($file) {
        $extension = substr($file,strripos($file, ".")+1);
        $type = strtolower($extension);
        if($type == 'jpeg') {$type = 'jpg';}
        $mime = "image/{$type}";
            
        $contents = file_get_contents($file);
        //$base64 = base64_encode($contents);
        
        return array("data"=>$contents,"mime"=>$mime);
    }
}
?>